const express = require("express");
const router = express.Router();

const userController = require("./../controllers/userController")
const auth = require("./../auth")


// http://localhost:4000/api/users

router.get("/email-exists", (req, res) => {

	userController.checkEmail(req.body).then( result => res.send(result))
})



router.post("/register", (req, res) => {
    userController.register(req.body).then(result => res.send(result))
})


router.get("/", (req, res) => {
    userController.getAllUsers().then(result => res.send(result))
})


router.post("/login", (req, res) => {
    userController.login(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)
    
    userController.getProfile(userData).then(result => res.send(result))
    //console.log(userData)
})




module.exports = router;