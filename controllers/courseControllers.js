
const Course = require("./../models/Course");

module.exports.createCourse = (reqBody) => {
	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllCourses = () => {

	return Course.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}

module.exports.getSpecificCourse = (reqBody) => {
	return Course.findOne({courseName: reqBody }).then((result, err) => {
		if (result === null) {
			return `Course not existing`
		} else {
		if(result){
			return result
		} else {
			return error
			}
		}
	})
}

module.exports.getCourseById = (params) => {

	 return Course.findById(params).then((result, err) => {
		if (result === null) {
			return `Course not existing`
		} else {
		if(result){
			return result
		} else {
			return error
			}
		}
	 })
}

module.exports.archiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: false
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus, {new: true}).then((result, err) => {
		if(result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}

module.exports.unarchiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: true
	}

	return Course.findOneAndUpdate({courseName: reqBody}, courseStatus).then((result, err) => {
		if(result == null) {
			return `Course not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}

module.exports.archiveCourseById = (params) => {
	return Course.findByIdAndUpdate(params, {isActive: false}).then((result, err) => {

		if(result == null){
			return `Course not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}

	})
}

module.exports.unarchiveCourseById = (params) => {
	return Course.findByIdAndUpdate(params, {isActive: true}).then((result, err) => {

		if(result == null){
			return `Course not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}

	})
}

module.exports.deleteCourse = (name) => {
	return Course.findOneAndDelete({courseName: name}).then((result, err) => {
		if(result == null){
			return `Course not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}

module.exports.deleteCourseById = (id) => {
	return Course.findByIdAndDelete(id).then((result, err) => {
		if(result == null){
			return `Course not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}

