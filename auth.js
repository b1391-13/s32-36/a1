//jsonwebtoken
const jwt = require("jsonwebtoken")

const secret = "CourseBookingAPI"

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret, {});

}

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization
    
    token = token.slice(7, token.length)
    //console.log(token)
    //console.log(typeof token)

    if(typeof token !== "undefined"){
        return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return res.send({auth: "failed"})
            } else {
                next()
            }
        })

    }
    
}

module.exports.decode = (token) => {
    
  token = token.slice(7, token.length)
   //console.log(token)
   if(typeof token !== "undefined"){
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return null
            } else {
                return jwt.decode(token, {complete: true}).payload
            }
        })
    }
}